export interface News {
    _id: String;
    title: String;
    url: String;
    created_at: string;
    createdAt: String;
    updatedAt: String;
    __v: number;
}