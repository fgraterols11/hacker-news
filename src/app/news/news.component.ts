import { Component, OnInit } from '@angular/core';
import { NewsService } from './services/news.service';
import { News } from './model/new.model';
import { DatePipe } from '@angular/common';
import { MatSnackBar} from '@angular/material/snack-bar';



@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  _news: News[];
  constructor(
    public NewService: NewsService,
    public datePipe: DatePipe,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getNews();
  }

  async getNews() {
    await this.NewService.getNews()
    .subscribe((response: News[]) => {
      this._news = response;
      this.sortData();
      this._news.forEach( element => {
        element.created_at = this.getDate(element.created_at);
      });
    }, err => {
      console.log(err);
    });
  }

  async deleteNew(id) {
    await this.NewService.deleteNews(id)
    .subscribe((response: any) => {
      if(response.code = '200') {
        this._snackBar.open('New deleted', 'Close', {
          duration: 3000
        });
      }
    }, err => {
      this._snackBar.open('Error deleting new', 'Close', {
        duration: 3000
      });
    });
  }

  getDate(created_at) { 
    var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"];
    let actualDate = new Date();
    let beforeDate = new Date();
    beforeDate.setDate(actualDate.getDate() - 1);
    let date = new Date(created_at);
    let text = '';
    if (this.datePipe.transform(date,"dd-MM-yyyy") === this.datePipe.transform(actualDate,"dd-MM-yyyy")) {
      text = this.datePipe.transform(date, 'shortTime')
    } else if (this.datePipe.transform(date,"dd-MM-yyyy") === this.datePipe.transform(beforeDate,"dd-MM-yyyy")) {
      text = 'Yesterday'
    } else {
      text = month[date.getMonth()] + ' ' + date.getUTCDate();
    }
     console.log(text);
     return text;
  }

  

  sortData() {
    return this._news.sort((a, b) => {
      return <any>new Date(b.created_at) - <any>new Date(a.created_at);
    });
  }
}
